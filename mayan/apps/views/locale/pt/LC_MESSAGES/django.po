# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Roberto Rosario, 2020
# Emerson Soares <on.emersonsoares@gmail.com>, 2020
# 425fe09b3064b9f906f637fff94056ae_a00ea56 <0fa3588fa89906bfcb3a354600956e0e_308047>, 2020
# 
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-03-28 13:02+0000\n"
"PO-Revision-Date: 2020-09-27 06:47+0000\n"
"Last-Translator: 425fe09b3064b9f906f637fff94056ae_a00ea56 <0fa3588fa89906bfcb3a354600956e0e_308047>, 2020\n"
"Language-Team: Portuguese (https://www.transifex.com/rosarior/teams/13584/pt/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pt\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: apps.py:15 settings.py:7
msgid "Views"
msgstr ""

#: forms.py:24
msgid "Selection"
msgstr "Seleção"

#: forms.py:100
msgid "None"
msgstr "Nenhum"

#: generics.py:150
msgid ""
"Select entries to be removed. Hold Control to select multiple entries. Once "
"the selection is complete, click the button below or double click the list "
"to activate the action."
msgstr ""

#: generics.py:155
msgid ""
"Select entries to be added. Hold Control to select multiple entries. Once "
"the selection is complete, click the button below or double click the list "
"to activate the action."
msgstr ""

#: generics.py:288
msgid "Add all"
msgstr ""

#: generics.py:297
msgid "Add"
msgstr "Adicionar"

#: generics.py:307
msgid "Remove all"
msgstr ""

#: generics.py:316
msgid "Remove"
msgstr "Remover"

#: generics.py:532
#, python-format
msgid "Duplicate data error: %(error)s"
msgstr ""

#: generics.py:553
#, python-format
msgid "%(object)s not created, error: %(error)s"
msgstr ""

#: generics.py:566
#, python-format
msgid "%(object)s created successfully."
msgstr ""

#: generics.py:605
#, python-format
msgid "%(object)s not deleted, error: %(error)s."
msgstr ""

#: generics.py:614
#, python-format
msgid "%(object)s deleted successfully."
msgstr ""

#: generics.py:750
#, python-format
msgid "%(object)s not updated, error: %(error)s."
msgstr ""

#: generics.py:761
#, python-format
msgid "%(object)s updated successfully."
msgstr ""

#: mixins.py:324
#, python-format
msgid "No %(verbose_name)s found matching the query"
msgstr ""

#: mixins.py:349
#, python-format
msgid "Unable to perform operation on object %(instance)s; %(exception)s."
msgstr ""

#: mixins.py:351
#, python-format
msgid "Operation performed on %(object)s."
msgstr ""

#: mixins.py:352
#, python-format
msgid "Operation performed on %(count)d object."
msgstr ""

#: mixins.py:353
#, python-format
msgid "Operation performed on %(count)d objects."
msgstr ""

#: mixins.py:354
#, python-format
msgid "Perform operation on %(object)s."
msgstr ""

#: mixins.py:355
#, python-format
msgid "Perform operation on %(count)d object."
msgstr ""

#: mixins.py:356
#, python-format
msgid "Perform operation on %(count)d objects."
msgstr ""

#: mixins.py:438
msgid "Object"
msgstr "Objeto"

#: settings.py:12
msgid "The number objects that will be displayed per page."
msgstr ""

#: templatetags/views_tags.py:28
msgid "Confirm delete"
msgstr "Confirmar eliminação"

#: templatetags/views_tags.py:33
#, python-format
msgid "Edit %s"
msgstr ""

#: templatetags/views_tags.py:36
msgid "Confirm"
msgstr "Confirmar"

#: templatetags/views_tags.py:40
#, python-format
msgid "Details for: %s"
msgstr ""

#: templatetags/views_tags.py:44
#, python-format
msgid "Edit: %s"
msgstr ""

#: templatetags/views_tags.py:48
msgid "Create"
msgstr "Criar"
