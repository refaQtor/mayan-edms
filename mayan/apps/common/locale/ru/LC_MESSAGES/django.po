# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Translators:
# Alex Chigrin <chalex84@mail.ru>, 2020
# Ilya Pavlov <spirkaa@gmail.com>, 2021
# mizhgan <i@mizhgan.ru>, 2018
# lilo.panic, 2016
# OLeg Si <olegsm35@gmail.com>, 2020
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-03-28 13:01+0000\n"
"PO-Revision-Date: 2021-03-01 08:55+0000\n"
"Last-Translator: Ilya Pavlov <spirkaa@gmail.com>\n"
"Language-Team: Russian (http://www.transifex.com/rosarior/mayan-edms/language/ru/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ru\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);\n"

#: apps.py:131 permissions.py:5 settings.py:13
msgid "Common"
msgstr "Общий"

#: classes.py:456
msgid "Model properties"
msgstr "Свойства модели"

#: classes.py:461
msgid "Model fields"
msgstr "Поля модели"

#: classes.py:504
msgid "Model related fields"
msgstr "Связанные поля модели"

#: classes.py:509
msgid "Model reverse fields"
msgstr "Обратные поля модели"

#: dependencies.py:195
msgid "Used to allow offline translation of the code text strings."
msgstr "Используется для разрешения автономного перевода текстовых строк кода."

#: dependencies.py:204
msgid "Provides style checking."
msgstr "Обеспечивает проверку стиля."

#: dependencies.py:209
msgid "Command line environment with autocompletion."
msgstr "Среда командной строки с автодополнением."

#: dependencies.py:214
msgid "Checks proper formatting of the README file."
msgstr "Проверяет правильность форматирования файла README."

#: links.py:31
msgid "About this"
msgstr "О программе"

#: links.py:34
msgid "Get the book"
msgstr "Купить книгу"

#: links.py:39
msgid "Locale profile"
msgstr "Профиль локали"

#: links.py:44
msgid "Edit locale profile"
msgstr "Редактировать локаль"

#: links.py:49
msgid "Documentation"
msgstr "Документация"

#: links.py:52
msgid "Forum"
msgstr "Форум"

#: links.py:56 views.py:106
msgid "License"
msgstr "Лицензия"

#: links.py:61
msgid "Copy"
msgstr "Копировать"

#: links.py:64
msgid "Setup"
msgstr "Настройки"

#: links.py:67
msgid "Source code"
msgstr "Исходный код"

#: links.py:71
msgid "Online store"
msgstr "Магазин"

#: links.py:75
msgid "Get support"
msgstr "Получить поддержку"

#: links.py:79 queues.py:9 views.py:175
msgid "Tools"
msgstr "Инструменты"

#: literals.py:18
msgid ""
"This feature has been deprecated and will be removed in a future version."
msgstr "Эта функция устарела и будет удалена в будущей версии."

#: literals.py:21
msgid ""
"Your database backend is set to use SQLite. SQLite should only be used for "
"development and testing, not for production."
msgstr "В качестве базы данных задан SQLite. SQLite не должен использоваться в рабочем окружении, только для разработки и тестирования!"

#: literals.py:30
msgid "Days"
msgstr "Дни"

#: literals.py:31
msgid "Hours"
msgstr "Часы"

#: literals.py:32
msgid "Minutes"
msgstr "Минуты"

#: menus.py:8
msgid "System"
msgstr "Система"

#: menus.py:10 menus.py:11
msgid "Facet"
msgstr "Грань"

#: menus.py:14
msgid "Actions"
msgstr "Действия"

#: menus.py:15
msgid "Related"
msgstr "Связанный"

#: menus.py:16
msgid "Secondary"
msgstr "Вторичный"

#: menus.py:21 models.py:25
msgid "User"
msgstr "Пользователь"

#: models.py:29
msgid "Timezone"
msgstr "Часовой пояс"

#: models.py:32
msgid "Language"
msgstr "Язык"

#: models.py:38
msgid "User locale profile"
msgstr "Локаль профиля пользователя"

#: models.py:39
msgid "User locale profiles"
msgstr "Локаль профилей пользователей"

#: permissions.py:8
msgid "Copy object"
msgstr "Копировать объект"

#: queues.py:7
msgid "Default"
msgstr "По умолчанию"

#: settings.py:19
msgid ""
"Time to delay background tasks that depend on a database commit to "
"propagate."
msgstr "Время задержки фоновых задач, зависящих от процесса распространения записанных в БД данных."

#: settings.py:26
msgid ""
"A list of strings designating all applications that are to be removed from "
"the list normally installed by Mayan EDMS. Each string should be a dotted "
"Python path to: an application configuration class (preferred), or a package"
" containing an application."
msgstr "Список строк, обозначающих приложения, которые должны быть удалены из списка, стандартно устанавливаемого Mayan EDMS. Каждая строка должна быть разделенным точками путем Python к: классу конфигурации приложения (предпочтительно) или пакету, содержащему приложение."

#: settings.py:36
msgid ""
"A list of strings designating all applications that are installed beyond "
"those normally installed by Mayan EDMS. Each string should be a dotted "
"Python path to: an application configuration class (preferred), or a package"
" containing an application."
msgstr "Список строк, обозначающих приложения, которые установлены помимо тех, которые обычно устанавливаются Mayan EDMS. Каждая строка должна быть разделенным точками путем Python к: классу конфигурации приложения (предпочтительно) или пакету, содержащему приложение."

#: settings.py:45
msgid ""
"Name of the view attached to the brand anchor in the main menu. This is also"
" the view to which users will be redirected after log in."
msgstr "Название представления, прикрепленного к названию сайта в главном меню. Это также представление, на которое пользователи будут перенаправлены после входа в систему."

#: settings.py:53
msgid "Name to be displayed in the main menu."
msgstr "Название сайта, отображаемое в главном меню."

#: settings.py:59
msgid "URL of the installation or homepage of the project."
msgstr "URL-адрес установки или домашней страницы проекта."

#: settings.py:64
msgid "Base URL path to use for all views."
msgstr "Базовый URL-путь для использования во всех представлениях."

#: validators.py:44
msgid "Enter a valid JSON value."
msgstr "Введите допустимое JSON-значение."

#: validators.py:68
msgid "Enter a valid YAML value."
msgstr "Введите допустимое YAML-значение."

#: validators.py:84
msgid ""
"Enter a valid 'internal name' consisting of letters, numbers, and "
"underscores."
msgstr "Введите допустимое \"внутреннее имя\", состоящее из латинских букв, цифр и знака подчеркивания."

#: views.py:29
msgid "About"
msgstr "О программе"

#: views.py:42
msgid "Current user locale profile details"
msgstr "Настройки локали текущего пользователя"

#: views.py:48
msgid "Edit current user locale profile details"
msgstr "Редактировать настройки локали текущего пользователя"

#: views.py:97
msgid "Dashboard"
msgstr "Дашборд"

#: views.py:118
#, python-format
msgid "Fields to be copied: %s"
msgstr "Будут скопированы поля: %s"

#: views.py:127
#, python-format
msgid "Make a copy of %(object_name)s \"%(object)s\"?"
msgstr "Сделать копию %(object_name)s \"%(object)s\"?"

#: views.py:136
msgid "Object copied successfully."
msgstr "Объект успешно скопирован."

#: views.py:153
msgid ""
"No results here means that don't have the required permissions to perform "
"administrative task."
msgstr "Отсутствие элементов здесь означает, что у вас нет необходимых разрешений для выполнения административных задач."

#: views.py:156
msgid "No setup options available."
msgstr "Нет доступных для настройки параметров."

#: views.py:160
msgid "Setup items"
msgstr "Настройки"

#: views.py:162
msgid "Here you can configure all aspects of the system."
msgstr "Здесь вы можете настроить все аспекты системы."

#: views.py:177
msgid "These modules are used to do system maintenance."
msgstr "Эти модули используются для обслуживания системы."
